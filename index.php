<?php include 'header.php' ?>;

<h1 class="text-center">PHP Test Project</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 col-md-offset-3">
            <?php include 'php/model.php' ?>
            <?php
            //display information:
                echo '<table class="marg">';
                echo '<thead>';
                echo '<tr>';
                echo '<th>Name</th>';
                echo '<th>Year</th>';
                echo '<th>Directored</th>';
                echo '<th>Budget</th>';
                echo '<th>Runtime</th>';
                echo '<th class="ganre">Ganre</th>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';

                $query = new Query();
                $firstResult = $query->firstResult();

                foreach($firstResult as $row)
                {
                    echo '<tr>';
                    echo '<td>' . $row['name'] . '</td>';
                    echo '<td>' . $row['year'] . '</td>';
                    echo '<td>' . $row['directed_by'] . '</td>';
                    echo '<td>' . $row['budget'] . '</td>';
                    echo '<td>' . $row['runtime'] . '</td>';
                    echo '<td class="ganre">' . $row['ganre'] . '</td>';
                    echo '</tr>';
                }

                echo '</tbody>';
                echo '</table>';

            ?>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>