<?php include 'header.php' ?>

<h1 class="text-center">Results</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-7 col-md-offset-4">
            <?php include 'php/model.php' ?>
            <?php
                echo '<table class="marg" border="1">';
                echo '<thead>';
                echo '<tr>';
                echo '<th>Studio Name</th>';
                echo '<th>Actor Name</th>';
                echo '<th>Films count</th>';
                echo '</thead>';
                echo '<tbody>';

                $thirdResult = new Query();
                foreach ($thirdResult->thirdResult() as $row)
                {
                    echo '<tr>';
                    echo '<td>' . $row['studio_name'] . '</td>';
                    echo '<td>' . $row['actor_name'] . '</td>';
                    echo '<td>' . $row['films_count'] . '</td>';
                    echo '</tr>';
                }

                echo '</tbody>';
                echo '</table>';
            ?>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>