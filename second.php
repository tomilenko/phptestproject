<?php include 'header.php' ?>

<h1 class="text-center">Choice Studio</h1>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 col-md-offset-5">
            <?php include 'php/model.php' ?>
            <?php
                // declare dropdown variable.
                $dropdown = "<select name='items' class='select'>";
                $secondResult = new Query();

                // start loop; as long as there is data in the column.
                foreach ($secondResult->secondResult() as $row)
                {
                    $dropdown .= "\r\n<option value='{$row['name']}'>{$row['name']}</option>";
                }
                $dropdown .= "\r\n</select>";
            ?>

            <form method="post" action="/action.php">
                <?php echo $dropdown;?>
                <input type="submit" value="Submit">
            </form>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>;