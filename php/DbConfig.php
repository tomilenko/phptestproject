<?php

class DbConfig {

    protected static $_host = '127.0.0.1';
    protected static $_userName = 'root';
    protected static $_password = 'root';
    protected static $_dbName = 'filmography';

    public static function getDbConfig(){
        return array(
            'host' => self::$_host,
            'user_name' => self::$_userName,
            'password' => self::$_password,
            'db_name' => self::$_dbName
        );
    }

}