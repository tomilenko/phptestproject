<?php

include_once 'DbConfig.php';

class DatabaseConnection {

    protected static $connection;

    public static function getConnection(){

        if(!isset(self::$connection)){

            $dbConfig = DbConfig::getDbConfig();

            $connectionString = sprintf(
                'mysql:dbname=%s;host=%s',
                (string) $dbConfig['db_name'],
                (string) $dbConfig['host']
            );

            $connection = new PDO(
                $connectionString,
                $dbConfig['user_name'],
                $dbConfig['password']
            );

            if(!$connection)
            {
                die ("Cannot connect to the database");
            }

            self::$connection = $connection;
        }
        return self::$connection;
    }


}