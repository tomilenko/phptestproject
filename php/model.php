<?php
include_once 'DatabaseConnection.php';

class Query extends  DatabaseConnection  {

    public function firstResult()
    {
        try {
            $connection = $this->getConnection();
            $query = "SELECT * FROM films";
            $stmt = $connection->prepare($query);
            $stmt->execute();

        } catch (Exception $e) {
            echo "Error.";
            exit;
        }

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }



    public function secondResult()
    {
        try {
            $connection = $this->getConnection();

            $query = "SELECT * FROM studios";
            $stmt = $connection->prepare($query);
            $stmt->execute();

        } catch (Exception $e) {
            echo "Error.";
            exit;
        }

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function thirdResult()
    {
        $choiceCompany = $_POST["items"];

        if(isset($choiceCompany))
        {
            $params = array (
                'companyName' => $choiceCompany
            );
            try {
                $connection = $this->getConnection();

                $query = "SELECT s.name AS studio_name, CONCAT(a.name, ' ', a.surname) AS actor_name, a.id_actor, COUNT(af.film_id) AS films_count
                                            FROM studios AS s
                                            INNER JOIN film_company AS fc ON s.id_studio = fc.studio_id
                                            INNER JOIN actors_film AS af ON fc.film_id = af.film_id
                                            INNER JOIN actors AS a ON a.id_actor = af.actor_id
                                        WHERE s.name = :companyName
                                        GROUP BY af.actor_id";
                $stmt = $connection->prepare($query);

                if (!$stmt->execute ($params)) {
                    echo "Error!";
                }

            } catch (Exception $e) {
                echo "Error.";
                exit;
            }

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
    }
}
