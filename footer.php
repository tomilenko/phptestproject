<div class="footer navbar-fixed-bottom">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <p class="text-muted">All rights reserved <span class="glyphicon glyphicon-copyright-mark"></span></p>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>